#!/bin/bash
#title           :aur-autobuilder.bash
#description     :Build aur packages if required and add them to my repo.
#		 :Generate a diff of the PKGBUILD and send me a link via
#		 :telegram.
#
#author          :Aaron Coach
#notes           :

# sendNotification function
source ./general-notification.sh

# Build here
workingPath="~/Builds/aur-autobuild"

# Where to put the diffs
webPath="/home/repo/web/autobuilder"

# Syntax highlighting
header='<link rel="stylesheet" href="/autobuilder/highlight/styles/darcula.css">
<script src="/autobuilder/highlight/highlight.pack.js">></script>
<script>hljs.initHighlightingOnLoad();</script>
<pre>'

footer='</code></pre>'

# List of packages to auto build
packages=(
  "linux-lqx"
  "kmozillahelper"
  "firefox-kde-opensuse"
  "glew1.13"
  "ncurses5-compat-libs"
  "rpcs3"
)

# Update pacman database for version check. Also make sure
# we have the current version of libs to build against
sudo pacman -Syu --noconfirm

if [ ! -d "${workingPath}" ]; then
    mkdir -p "${workingPath}"
fi

# Get repo version via pacman and aur version via cower
getVersions() {
    currentVersion=$(pacman -Si $1 | grep Version | awk '{print $3}')
    aurVersion=$(cower --ignorerepo=awc --info $1 | grep Version | awk '{print $3}')
}

downloadPackage() {
    cower -f --download --ignorerepo=awc --target $workingPath $1
    newPKGBUILD=${workingPath}/${1}/PKGBUILD
    oldPKGBUILD=${workingPath}/.old/${1}/PKGBUILD
}

for package in ${packages[@]}; do
    getVersions $package
    
    if [ "$currentVersion" != "$aurVersion" ]; then
        downloadPackage $package
        if [ -f $oldPKGBUILD ]; then
            difference='<code class="diff">'"$(diff ${newPKGBUILD} ${oldPKGBUILD})"
        else
            difference='<code class="bash">'"$(cat ${newPKGBUILD})"
        fi
        
        echo "${header}${difference}${footer}" | sudo -H -u repo tee "$webPath/$package-${aurVersion}.html"
        
        if [ ! -d "${workingPath}/.old/${package}" ]; then
            mkdir -p "${workingPath}/.old/${package}"
        fi
        
        pushd "${workingPath}/${package}"
        if makepkg -Cs --noconfirm; then
            for pkg in $( ls *.pkg.tar.xz ); do
                sudo mv ${pkg} /home/repo/web
                sudo chown repo:users /home/repo/web/${pkg}
                sudo -H -u repo bash -c "repo-add -R /home/repo/web/awc.db.tar.xz /home/repo/web/${pkg}"
            done
            
            cp "${newPKGBUILD}" "${oldPKGBUILD}"
            
            sendNotification "AUR Auto-Builder:"\
                "${package} updated to [${aurVersion}](https://repo.awc.id.au/autobuilder/${package}-${aurVersion}.html)"
        else
            sendNotification "AUR Auto-Builder:"\
                "Error building $package"
        fi
        
        popd >/dev/null
    fi
done
